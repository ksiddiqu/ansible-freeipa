#!/bin/sh -eux
 
# Use pip3 to install ansible (2.8+)
pip3 install "ansible>=2.8"
 
ansible-playbook -i inventory install-server.yml -v --connection=local
